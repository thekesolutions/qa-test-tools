package QohA::File::Specific::Kohastructure;

use Modern::Perl;
use Moo;
extends 'QohA::File';

sub run_checks {
    my ($self, $cnt) = @_;

    my $r = $self->check_charset_collate();
    $self->SUPER::add_to_report('charset_collate', $r);

    $r = $self->check_boolean_vs_tinyint();
    $self->SUPER::add_to_report('boolean_vs_tinyint', $r);

    $r = $self->check_tinyint_has_boolean_flag();
    $self->SUPER::add_to_report('tinyint_has_boolean_flag', $r);

    return $self->SUPER::run_checks($cnt);
}

sub check_charset_collate {
    my ($self) = @_;
    return 0 unless -e $self->path;

    open( my $fh, $self->path )
      or die "I cannot open $self->path ($!)";

    my @bad_charset_collate;
    my $current_table;
    while ( my $line = <$fh> ) {
        if ( $line =~
            m|CREATE\s+TABLE\s+(IF NOT EXISTS)?\s*`?(?<cur_table>[\w_]*)`?\s*\(\s*| )
        {
            $current_table = $+{cur_table};
            next;
        }
        next unless $line =~ m|CHARSET=utf8|;
        next if $line =~ m|CHARSET=utf8mb4|
            and $line =~ m|utf8mb4_unicode_ci|;
        push @bad_charset_collate, $current_table;
    }
    close $fh;
    return 1 unless @bad_charset_collate;
    my @errors;
    push @errors,
      "The table $_ does not have the current charset collate (see bug 18336)"
      for @bad_charset_collate;
    return \@errors;
}

sub check_boolean_vs_tinyint {
    my ($self) = @_;
    return 0 unless -e $self->path;

    open( my $fh, $self->path )
      or die "I cannot open $self->path ($!)";

    my (@booleans, @int_1, $current_table);
    while ( my $line = <$fh> ) {
        if ( $line =~
            m|CREATE\s+TABLE\s+(IF NOT EXISTS)?\s*`?(?<cur_table>[\w_]*)`?\s*\(\s*| )
        {
            $current_table = $+{cur_table};
            next;
        }
        if ( $line =~ m|^\s*`?(?<column_name>[_\w]+)`?.*BOOLEAN|i ) {
            push @booleans, { table => $current_table, column => $+{column_name}, line => $line };
        }
        elsif ( $line =~ m|^\s*`?(?<column_name>[_\w]+)`?.*\sINT\(1\)|i ) {
            push @int_1, { table => $current_table, column => $+{column_name}, line => $line };
        }

    }
    close $fh;

    return 1 unless @booleans or @int_1;

    my @errors;
    push @errors,
      sprintf ("The new column (%s) for table %s is using BOOLEAN as type, must be TINYINT(1) instead, see the SQL12 coding guideline", $_->{column}, $_->{table})
      for @booleans;

    push @errors,
      sprintf ("WARNING - The new column (%s) for table %s is using INT(1) as type, must be TINYINT(1) if it has a boolean purpose, see the SQL12 coding guideline", $_->{column}, $_->{table})
      for @int_1;

    return \@errors;
}

sub check_tinyint_has_boolean_flag {
    my ($self) = @_;
    return 0 unless -e $self->path;

    open( my $fh, $self->path )
      or die "I cannot open $self->path ($!)";

    my @tinyints;
    my $current_table;
    while ( my $line = <$fh> ) {
        if ( $line =~
            m|CREATE\s+TABLE\s+(IF NOT EXISTS)?\s*`?(?<cur_table>[\w_]*)`?\s*\(\s*| )
        {
            $current_table = $+{cur_table};
            next;
        }
        if ( $line =~ m|^\s*`?(?<column_name>[\w_]+)`?.*TINYINT\(1\)|i ) {
            push @tinyints, { table => $current_table, column => $+{column_name}, line => $line };
        }
    }
    close $fh;
    return 1 unless @tinyints;

    my @errors;
    for my $tinyint (@tinyints) {
        my $git_grep_cmd = sprintf q[git grep -l '__PACKAGE__\->table("%s")' Koha/Schema/Result], $tinyint->{table};
        my @schema_files = `$git_grep_cmd`;
        unless ( @schema_files ) { warn "No schema file found for table " . $tinyint->{table}; next}
        my $koha_schema_file = $schema_files[0];
        chomp $koha_schema_file;
        open ( my $fh, $koha_schema_file )
            or warn sprintf "Cannot open Schema file %s for table %s (%s)", $koha_schema_file, $tinyint->{table}, $! || next;
        $git_grep_cmd = sprintf "git grep '+%s' %s | grep 'is_boolean => '", $tinyint->{column}, $koha_schema_file; # Note: This will not support definitions on several lines
        my @matches = `$git_grep_cmd`;
        push @errors,
          sprintf "The new column %s.%s is using TINYINT(1) as type but is not defined as boolean in the schema file (%s), see the SQL12 coding guideline",
            $tinyint->{table}, $tinyint->{column}, $koha_schema_file
          unless @matches;
    }
    return \@errors;
}

1;

=head1 AUTHOR
Mason James <mtj at kohaaloha.com>
Jonathan Druart <jonathan.druart@biblibre.com>

=head1 COPYRIGHT AND LICENSE

This software is Copyright (c) 2012 by KohaAloha and BibLibre

This is free software, licensed under:

  The GNU General Public License, Version 3, June 2007
=cut
